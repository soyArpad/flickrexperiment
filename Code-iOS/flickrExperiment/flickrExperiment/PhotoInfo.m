//
//  PhotoInfo.m
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import "PhotoInfo.h"

@implementation PhotoInfo

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    PhotoInfo *photoInfo = [PhotoInfo new];
    
    photoInfo.uploadedDate = [NSDate dateWithTimeIntervalSince1970:[dictionary[@"dateuploaded"] doubleValue]];
    photoInfo.username = dictionary[@"owner"][@"username"];
    
    return photoInfo;
}

- (NSString *)uploadedDateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm/dd/yyyy"];
    
    return [formatter stringFromDate:_uploadedDate];
}

@end
