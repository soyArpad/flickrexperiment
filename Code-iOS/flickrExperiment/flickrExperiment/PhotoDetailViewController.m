//
//  PhotoDetailViewController.m
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import "PhotoDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FlickrHelper.h"
#import "Photo.h"
#import "PhotoInfo.h"
#import "LargePhotoViewController.h"

@interface PhotoDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadedLabel;

@end

@implementation PhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.photoImageView.image = self.photoLowResolution;
    NSURL *url = self.photo.largeImageURL;
    [self.photoImageView sd_setImageWithURL:url placeholderImage:self.photoLowResolution];
    self.titleLabel.text = self.photo.titlePhoto;
    [[FlickrHelper sharedFlickrHelper] getDetailsForPhoto:self.photo.idPhoto infoBlock:^(PhotoInfo *info, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.title = info.username;
                self.uploadedLabel.text = info.uploadedDateString;
            });
        }
    }];
}
- (IBAction)showFullScreenPhotoPressed:(UIButton *)sender {
    LargePhotoViewController *largePhotoVC = [self.storyboard instantiateViewControllerWithIdentifier:LARGE_PHOTO_VC];
    largePhotoVC.photoLR = self.photoLowResolution;
    largePhotoVC.photo = self.photo;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:largePhotoVC];
    [self presentViewController:navController animated:YES completion:nil];
}


@end
