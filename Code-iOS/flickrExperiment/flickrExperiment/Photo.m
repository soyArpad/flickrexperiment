//
//  Photo.m
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import "Photo.h"
#import <FlickrKit.h>

@interface Photo ()

@property (nonatomic, strong) NSDictionary *photoDictionary;

@end

@implementation Photo

+ (instancetype)photoWithDictionary:(NSDictionary *)photoDictionary;
{
    Photo *photo = [Photo new];
    
    photo.idPhoto = photoDictionary[@"id"];
    photo.titlePhoto = photoDictionary[@"title"];
    photo.photoDictionary = photoDictionary;
    
    return photo;
}

- (NSURL *)thumbnailURL
{
    return [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeThumbnail100 fromPhotoDictionary:self.photoDictionary];
}

- (NSURL *)largeImageURL
{
    return [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeLarge1024 fromPhotoDictionary:self.photoDictionary];
}

@end
