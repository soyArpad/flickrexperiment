//
//  PhotoCell.h
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import <UIKit/UIKit.h>

@class Photo;

@interface PhotoCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *photoImageView;

- (void)configureCellWithPhoto:(Photo *)photo;

@end
