//
//  Photo.h
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import <Foundation/Foundation.h>

@interface Photo : NSObject

@property (nonatomic, strong) NSString *idPhoto;
@property (nonatomic, strong) NSString *titlePhoto;
@property (nonatomic, readonly) NSURL *thumbnailURL;
@property (nonatomic, readonly) NSURL *largeImageURL;


+ (instancetype)photoWithDictionary:(NSDictionary *)photoDictionary;

@end
