//
//  LargePhotoViewController.m
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import "LargePhotoViewController.h"
#import "Photo.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface LargePhotoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@end

@implementation LargePhotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"FullScreen Image";
    
    NSURL *url = self.photo.largeImageURL;
    [self.photoImageView sd_setImageWithURL:url placeholderImage:self.photoLR];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)donePressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
