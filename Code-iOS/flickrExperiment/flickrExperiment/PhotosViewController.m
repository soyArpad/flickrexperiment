//
//  PhotosViewController.m
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import "PhotosViewController.h"
#import <FlickrKit.h>
#import "LoginFlickrViewController.h"
#import "PhotoCell.h"
#import "PhotoDetailViewController.h"
#import "PhotoResultsCollectionViewController.h"
#import "FlickrHelper.h"
#import "Photo.h"
#import "MainNavigationController.h"

@interface PhotosViewController () <PhotoResultsDelegate> {
    NSArray                                             *_photosArray;
    PhotoResultsCollectionViewController                *_photoResultsVC;
    int                                                 _page;
    int                                                 _totalPages;
    
}

@property (nonatomic, retain) FKFlickrNetworkOperation *todaysInterestingOp;
@property (nonatomic, retain) FKFlickrNetworkOperation *myPhotostreamOp;
@property (nonatomic, retain) FKDUNetworkOperation *completeAuthOp;
@property (nonatomic, retain) FKDUNetworkOperation *checkAuthOp;
@property (nonatomic, retain) FKImageUploadNetworkOperation *uploadOp;
@property (nonatomic, retain) NSString *userID;
@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation PhotosViewController

static NSString * const reuseIdentifier = @"PhotoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthenticateCallback:) name:CALLBACK_NOTIFICATION object:nil];
    
    [self.collectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    [self createSearchController];
    self.definesPresentationContext = YES;
    [self.collectionView addSubview:self.searchController.searchBar];
    [self.collectionView setContentOffset:CGPointMake(0, 44)];
    [self.searchController.searchBar sizeToFit];
    
    _page = 1;
    [self filterContentForSearchText:MAIN_SEARCH_TOPIC fromZero:YES];
}

- (void)createSearchController
{
    _photoResultsVC = [self.storyboard instantiateViewControllerWithIdentifier:PHOTO_RESULTS_VC];
    _photoResultsVC.delegate = self;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:_photoResultsVC];
    self.searchController.searchResultsUpdater = (id<UISearchResultsUpdating>)_photoResultsVC;
    self.searchController.searchBar.barTintColor = [UIColor colorWithRed:108/255.0f green:192/255.0f blue:255/255.0f alpha:1.0];
    self.searchController.searchBar.tintColor = [UIColor whiteColor];
}

#pragma mark - Actions

- (IBAction)reloadPressed:(UIBarButtonItem *)sender {
    if (_photosArray.count) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
    _page = 1;
    [self filterContentForSearchText:MAIN_SEARCH_TOPIC fromZero:YES];
}

- (IBAction)addPhotoPressed:(UIBarButtonItem *)sender {
        self.checkAuthOp = [[FlickrKit sharedFlickrKit] checkAuthorizationOnCompletion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    [self presentAddPhoto];
                } else {
                    [self authenticateUser];
                }
            });
        }];
}

#pragma mark - Add Photo

- (void)presentAddPhoto {
    MainNavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:UPLOAD_IMAGE_NAVC];
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - search

- (void)filterContentForSearchText:(NSString*)searchText fromZero:(BOOL)isFromZero
{
    if (isFromZero || _page < _totalPages) {
        self.collectionView.userInteractionEnabled = NO;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [[FlickrHelper sharedFlickrHelper] searchPhotosWithTag:searchText page:[NSString stringWithFormat:@"%i",_page] resultsPerPage:@"20" resultBlock:^(NSArray *results, int pages, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _totalPages = pages;
                self.collectionView.userInteractionEnabled = YES;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (!error && results) {
                    if (!isFromZero) {
                        NSMutableArray *auxArray = [[NSMutableArray alloc] initWithArray:_photosArray];
                        [auxArray addObjectsFromArray:results];
                        _photosArray = auxArray;
                    }
                    else
                        _photosArray = results;
                    
                    [self.collectionView reloadData];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            });
        }];
        _page++;
    }
}

#pragma mark - Auth

- (void) userAuthenticateCallback:(NSNotification *)notification {
    NSURL *callbackURL = notification.object;
    self.completeAuthOp = [[FlickrKit sharedFlickrKit] completeAuthWithURL:callbackURL completion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [self dismissViewControllerAnimated:YES completion:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentAddPhoto];
                    });
                }];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];[self dismissViewControllerAnimated:YES completion:nil];
            }
            
        });
    }];
}

- (void)authenticateUser
{
    LoginFlickrViewController *loginFlickrVC = [self.storyboard instantiateViewControllerWithIdentifier:LOGIN_FLICKR_VC];
    MainNavigationController *navController = [[MainNavigationController alloc] initWithRootViewController:loginFlickrVC];
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - Photos delegate

- (void)photoResultsVC:(PhotoResultsCollectionViewController *)photoResultsVC didSelectPhoto:(Photo *)photo withImage:(UIImage *)image
{
    PhotoDetailViewController *photoDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:PHOTO_DETAIL_VC];
    photoDetailVC.photo = photo;
    photoDetailVC.photoLowResolution = image;
    [self.navigationController pushViewController:photoDetailVC animated:YES];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    [cell configureCellWithPhoto:[_photosArray objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(59, 15, 15, 15);
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _photosArray.count-1) {
        [self filterContentForSearchText:MAIN_SEARCH_TOPIC fromZero:NO];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:PHOTO_DETAIL_SEGUE sender:self];
}

#pragma mark - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:PHOTO_DETAIL_SEGUE]) {
        PhotoDetailViewController *photoDetailVC = [segue destinationViewController];
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
        NSIndexPath *indexPath = indexPaths[0];
        PhotoCell *cell = (PhotoCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        UIImage *photoLR = cell.photoImageView.image;
        
        photoDetailVC.photo = [_photosArray objectAtIndex:indexPath.row];
        photoDetailVC.photoLowResolution = photoLR;
    }
}

@end
