//
//  AppDelegate.m
//  flickrExperiment
//
//  Created by Arpad Larrinaga on 7/22/15.
//  Copyright (c) 2015 Arpad. All rights reserved.
//

#import "AppDelegate.h"
#import <FlickrKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Flickr Config
    [[FlickrKit sharedFlickrKit] initializeWithAPIKey:@"d58ceca2fc613616d269f38589f561bd" sharedSecret:@"bc33e79034df7d4c"];
    
    // Set appareance
    [[UINavigationBar appearance] setBarTintColor:MAIN_COLOR];
    [[UINavigationBar appearance] setTintColor:SECUNDARY_COLOR];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: SECUNDARY_COLOR}];
    
    return YES;
}

- (BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSString *scheme = [url scheme];
    if([@"flickrmind" isEqualToString:scheme]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:CALLBACK_NOTIFICATION object:url userInfo:nil];
    }
    return YES;
}

@end
