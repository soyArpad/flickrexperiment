//
//  PhotoResultsCollectionViewController.h
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import <UIKit/UIKit.h>

@class Photo;
@class PhotoResultsCollectionViewController;

@protocol PhotoResultsDelegate <NSObject>

@required
- (void)photoResultsVC:(PhotoResultsCollectionViewController *)photoResultsVC didSelectPhoto:(Photo *)photo withImage:(UIImage *)image;

@end

@interface PhotoResultsCollectionViewController : UICollectionViewController

@property (nonatomic, assign) id<PhotoResultsDelegate> delegate;

@end
