//
//  FlickrHelper.m
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import "FlickrHelper.h"
#import <FlickrKit.h>
#import "Photo.h"
#import "PhotoInfo.h"

@implementation FlickrHelper

#pragma mark - Singleton

+ (instancetype)sharedFlickrHelper
{
    static FlickrHelper *sharedFlickrHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedFlickrHelper = [[FlickrHelper alloc] init];
    });
    return sharedFlickrHelper;
}

#pragma mark - Initializers

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Methods

- (void)searchPhotosWithTag:(NSString *)tag page:(NSString *)page resultsPerPage:(NSString *)resultsPerPage resultBlock:(SearchBlock)resultBlock {
    FKFlickrPhotosSearch *search = [[FKFlickrPhotosSearch alloc] init];
    search.text = tag;
    search.page = page;
    search.per_page = resultsPerPage;
    [[FlickrKit sharedFlickrKit] call:search completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                NSMutableArray *photoURLs = [NSMutableArray array];
                for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                    [photoURLs addObject:[Photo photoWithDictionary:photoDictionary]];
                }
                int pages = [[response valueForKeyPath:@"photos.pages"] intValue];
                if (resultBlock)
                    resultBlock(photoURLs, pages, error);
                
            } else {
                if (resultBlock)
                    resultBlock(nil, 1, error);
            }
        });
    }];
}

- (void)getDetailsForPhoto:(NSString *)photoId infoBlock:(GetInfoBlock)infoBlock; {
    FKFlickrPhotosGetInfo *getInfo = [[FKFlickrPhotosGetInfo alloc] init];
    getInfo.photo_id = photoId;
    [[FlickrKit sharedFlickrKit] call:getInfo completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                NSMutableArray *photoURLs = [NSMutableArray array];
                for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                    [photoURLs addObject:photoDictionary];
                }
                if (infoBlock)
                    infoBlock([PhotoInfo initWithDictionary:[response objectForKey:@"photo"]], error);
                
            } else {
                if (infoBlock)
                    infoBlock(nil, error);
                
            }
        });
    }];
}

@end
