//
//  UploadImageViewController.m
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import "UploadImageViewController.h"
#import <FlickrKit.h>
#import <MBProgressHUD.h>

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface UploadImageViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    NSURL                                   *_imageurl;
    BOOL                                    _imageSelected;
    MBProgressHUD                           *_hud;
    CGFloat                                 _animatedDistance;
}

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (nonatomic, retain) FKImageUploadNetworkOperation *uploadOp;

@end

@implementation UploadImageViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _imageSelected = NO;
}

#pragma mark - Actions

- (IBAction)selectImagePressed:(UIButton *)sender {
    [self selectImage];
}

- (void)selectImage
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose your source" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *fromLibrary = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
        });
    }];
    [alert addAction:fromLibrary];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertAction *fromCamera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePicker animated:YES completion:nil];
            });
        }];
        
        [alert addAction:fromCamera];
    }
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    if (self.uploadOp && self.uploadOp.executing)
        [self.uploadOp cancel];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)uploadPressed:(UIBarButtonItem *)sender {
    [self validateAndSendImage];
}

- (void)validateAndSendImage
{
    [self.titleTextField resignFirstResponder];
    [self.descriptionTextField resignFirstResponder];
    if(!_imageSelected)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select an image" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self selectImage];
            });
        }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(self.titleTextField.text.length == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title cannot be empty" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.titleTextField becomeFirstResponder];
            });
        }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(self.descriptionTextField.text.length == 0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Description cannot be empty" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.descriptionTextField becomeFirstResponder];
            });
        }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        
        [self uploadImage];
    }
}

#pragma mark - Textfield delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    _animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= _animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += _animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.titleTextField) {
        [self.descriptionTextField becomeFirstResponder];
    }
    else {
        [self validateAndSendImage];
    }
    return YES;
}

#pragma mark - Progress KVO

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat progress = [[change objectForKey:NSKeyValueChangeNewKey] floatValue];
        _hud.progress = progress;
    });
}

#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    _imageurl = [info objectForKey:UIImagePickerControllerReferenceURL];
    
    self.photoImageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    _imageSelected = YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Upload Image

- (void)uploadImage
{
    NSDictionary *uploadArgs = @{@"title": self.titleTextField.text, @"description": self.descriptionTextField.text, @"is_public": @"1", @"is_friend": @"0", @"is_family": @"0", @"hidden": @"2"};
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hud.mode = MBProgressHUDModeAnnularDeterminate;
    _hud.labelText = @"Uploading...";
    self.uploadOp =  [[FlickrKit sharedFlickrKit] uploadAssetURL:_imageurl args:uploadArgs completion:^(NSString *imageID, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Uploaded Image" message:imageID preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self dismissViewControllerAnimated:YES completion:nil];
                    });
                }];
                [alert addAction:okAction];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            [self.uploadOp removeObserver:self forKeyPath:@"uploadProgress" context:NULL];
            [_hud hide:YES];
        });
    }];
    [self.uploadOp addObserver:self forKeyPath:@"uploadProgress" options:NSKeyValueObservingOptionNew context:NULL];
}

@end
