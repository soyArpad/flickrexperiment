//
//  PhotoCell.m
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import "PhotoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <PureLayout.h>
#import "Photo.h"

@interface PhotoCell ()

@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@end

@implementation PhotoCell

- (void)configureCellWithPhoto:(Photo *)photo
{
    if (!self.photoImageView) {
        self.photoImageView = [[UIImageView alloc] initForAutoLayout];
        [self.contentView addSubview:self.photoImageView];
        
        [self.photoImageView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.spinner.center = CGPointMake(self.contentView.frame.size.width/2, self.contentView.frame.size.height/3);
        [self.contentView addSubview:self.spinner];
    }
    
    [self.spinner startAnimating];
    [self.photoImageView sd_setImageWithURL:photo.thumbnailURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.spinner stopAnimating];
        });
    }];
}

@end
