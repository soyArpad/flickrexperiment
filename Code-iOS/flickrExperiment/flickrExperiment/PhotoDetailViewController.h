//
//  PhotoDetailViewController.h
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import <UIKit/UIKit.h>

@class Photo;

@interface PhotoDetailViewController : UIViewController

@property (nonatomic, weak) UIImage *photoLowResolution;
@property (nonatomic, weak) Photo *photo;

@end
