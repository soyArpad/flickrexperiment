//
//  PhotoInfo.h
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import <Foundation/Foundation.h>

@interface PhotoInfo : NSObject

@property (nonatomic, copy) NSDate *uploadedDate;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, readonly) NSString *uploadedDateString;

+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
