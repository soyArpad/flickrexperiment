//
//  PhotoResultsCollectionViewController.m
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import "PhotoResultsCollectionViewController.h"
#import "PhotoCell.h"
#import "PhotoDetailViewController.h"
#import "FlickrHelper.h"
#import "Photo.h"

@interface PhotoResultsCollectionViewController ()<UISearchResultsUpdating, UISearchBarDelegate> {
    NSArray                         *_photosArray;
    NSString                        *_searchString;
    int                             _page;
    int                             _totalPages;
}

@end

@implementation PhotoResultsCollectionViewController

static NSString * const reuseIdentifier = @"PhotoCell";

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _page = 1;
    
    [self.collectionView registerClass:[PhotoCell class] forCellWithReuseIdentifier:reuseIdentifier];
}

#pragma mark - search

- (void)filterContentForSearchText:(NSString*)searchText fromZero:(BOOL)isFromZero
{
    if (isFromZero || _page < _totalPages) {
        self.collectionView.userInteractionEnabled = NO;
        [[FlickrHelper sharedFlickrHelper] searchPhotosWithTag:_searchString page:[NSString stringWithFormat:@"%i",_page] resultsPerPage:@"20" resultBlock:^(NSArray *results, int pages, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _totalPages = pages;
                self.collectionView.userInteractionEnabled = YES;
                if (!error && results) {
                    if (!isFromZero) {
                        NSMutableArray *auxArray = [[NSMutableArray alloc] initWithArray:_photosArray];
                        [auxArray addObjectsFromArray:results];
                        _photosArray = auxArray;
                    }
                    else
                        _photosArray = results;
                    
                    
                    [self.collectionView reloadData];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            });
        }];
        _page++;
    }
    
}

#pragma UISearchBarDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    _page = 1;
    if (_photosArray.count) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
    
    _searchString = searchController.searchBar.text;
    if (_searchString.length != 0)
        [self filterContentForSearchText:_searchString fromZero:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    [cell configureCellWithPhoto:[_photosArray objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *cell = (PhotoCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    UIImage *photoLR = cell.photoImageView.image;
    Photo *photo = [_photosArray objectAtIndex:indexPath.row];
    
    if (_delegate) {
            [_delegate photoResultsVC:self didSelectPhoto:photo withImage:photoLR];
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _photosArray.count-1) {
        [self filterContentForSearchText:_searchString fromZero:NO];
    }
}

@end
