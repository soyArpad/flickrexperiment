//
//  Macros.h
//  flickrExperiment
//
//  Created by Arpad Larrinaga on 7/23/15.
//  Copyright (c) 2015 Arpad. All rights reserved.
//

#ifndef flickrExperiment_Macros_h
#define flickrExperiment_Macros_h

#define MAIN_COLOR                                  [UIColor colorWithRed:108/255.0f green:192/255.0f blue:255/255.0f alpha:1.0]
#define SECUNDARY_COLOR                             [UIColor whiteColor]

#define CALLBACK_NOTIFICATION                       @"UserAuthCallbackNotification"

#define MAIN_SEARCH_TOPIC                           @"mind"

#define PHOTO_DETAIL_SEGUE                          @"photoDetailSegue"

#define PHOTO_DETAIL_VC                             @"PhotoDetailVC"
#define LOGIN_FLICKR_VC                             @"loginFlickr"
#define PHOTO_RESULTS_VC                            @"PhotoResults"
#define UPLOAD_IMAGE_NAVC                           @"UploadImageNavController"
#define LARGE_PHOTO_VC                              @"LargePhotoVC"

#endif
