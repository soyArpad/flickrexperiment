//
//  FlickrHelper.h
//  
//
//  Created by Arpad Larrinaga on 7/22/15.
//
//

#import <Foundation/Foundation.h>

@class PhotoInfo;

@interface FlickrHelper : NSObject

typedef void(^SearchBlock)(NSArray *results, int pages, NSError *error);
typedef void(^GetInfoBlock)(PhotoInfo *info, NSError *error);

+ (instancetype)sharedFlickrHelper;

- (void)searchPhotosWithTag:(NSString *)tag page:(NSString *)page resultsPerPage:(NSString *)resultsPerPage resultBlock:(SearchBlock)resultBlock;
- (void)getDetailsForPhoto:(NSString *)photoId infoBlock:(GetInfoBlock)infoBlock;

@end
