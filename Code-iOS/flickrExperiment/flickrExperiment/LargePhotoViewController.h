//
//  LargePhotoViewController.h
//  
//
//  Created by Arpad Larrinaga on 7/23/15.
//
//

#import <UIKit/UIKit.h>

@class Photo;

@interface LargePhotoViewController : UIViewController

@property (nonatomic, assign) Photo *photo;
@property (nonatomic, strong) UIImage *photoLR;

@end
